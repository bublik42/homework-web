﻿'use strict'
//1
////Создайте объект lab3, у которого есть следующие свойства:
////id -- номер вашей бригады
////students -- массив из строк, содержит фамилии и имена студентов выполнявших задание. 
//////количество строк -- количество человек в бригаде
////teacher - полное ФИО преподавателя.


var lab3 = {
  id: 6,
  students: ['korenevich', 'stefkin'],
  teacher: 'Безручко Алексей Николаевич'
};

//2
////Разработайте функцию checkProp, которая принимает в качестве параметров
////объект и название свойства
////если объект содержит указанное свойство, то функция возвращает значение этого свойства
////в противном случае функция возвращает false

// fix
function checkProp(obj, prop) {
  return prop in obj && obj[prop];
}


//3
////Добавьте в объект lab3 метод getAllProperties
////который принимает объект
////и возвращает строку, которая содержит имена всех свойств этого
////объекта записанные через ', ' в алфавитном порядке

lab3['getAllProperties'] = function(obj) {
  return Object.getOwnPropertyNames(obj).sort().join(', ');
};

//4
////Добавьте в объект lab3 метод clone object
////который принимает объект
////и возвращает его клона.

lab3['clone object'] = function(obj) {
  var result = {};
  Object.getOwnPropertyNames(obj).forEach(function(key){
    result[key] = obj[key];
  });
  return result;
};

//5
////Разработайте функцию addToBeginingOfArray которая принимает два аргумента. второй аргумент массив.
////функция добавляет первый аргумент в начало массива, переданного во втором аргументе.
////если массив не указан, то функция создает новый пустой массив и добавляет в него элемент.
////функция возвращает полученный массив.

function addToBeginingOfArray(obj, array) {
  array = array || [];
  array.unshift(obj);
  return array;
}

//6
////Разработайте функцию getLastElement которая принимает массив.
////функция возвращает последний элемент, удаляя его из массива

function getLastElement(array) {
  return (array || []).pop();
}

//7
////Разработайте функцию getFirstElement которая принимает массив.
////функция возвращает первый элемент, удаляя его из массива

function getFirstElement(array) {
  return (array || []).shift();
}

//8
////Разработайте функцию addToEndOfArray которая принимает любое количество аргументов.
////если первый аргумент массив, то все остальные аргументы добавляются в конец этого массива.
////Если первый аргумент не массив, то все аргументы функции добавляются в новый массив
////функция возвращает полученный массив.

function addToEndOfArray(first, ...rest) {
  if (Array.isArray(first)) {
    return first.concat(rest);
  } else {
    return Array.from(arguments);
  }
}

//9
////Разработайте функцию setSize, которая принимает массив в качеств первого аргумента
////если вторым аргументом передано положительное число, которое меньше чем длина массива,
////то функция укорачивает массив до указанной длины.
////В проитвном случае функция возвращает длину массива

function setSize(array, num) {
  if (Number.isInteger(num) && num >= 0 && num < array.length) {
    array.length = num;
  }
  return array.length;
}

//10
////Разработайте функцию remove3, которая принимает строку
//// и удаляет из этой строки 3 слова,
////которые занимают 3-5 места при сортировке слов по алфавиту
////иставшиеся слова возвращаются в отсортированном по алфавиту массиве.
////слова в строке разделяются пробелом.

function remove3(str) {
  const words = str.split(' ').sort();
  words.splice(2, 3);
  return words;
}

//11
////Разработайте функцию sortCars, которая принимает массив объектов car 
////с тремя полями brand - марка автомобиля, model - модель, year - год выпуска
////функция должна отсортировать автомобили по марке(по алфавиту), 
////если марки одинаковые, то по модели (по алфавиту),
////Если модели совпадают, то по убавынию года выпуска 
////функция sort у массива должна быть вызвана только один раз

function compare(a, b) {
  return a > b ? 1 : a < b ? -1 : 0;
}

function sortCars(cars) {
  return cars.sort(function(a, b) {
    return compare(a.brand, b.brand) ||
      compare(a.model, b.model) ||
      compare(a.year, b.year);
  });
}
//12
////Разработайте функцию updateCase, которая принимает массив объектов car 
////с тремя полями brand - марка автомобиля, model - модель, year - год выпуска
////не используя операторов цикла функция заменяет регистр всех строковых полей на верхний

//fix

function updateCase(cars) {
  cars.forEach(function(car){
    Object.getOwnPropertyNames(car).forEach(function(key){
      if (typeof car[key] == 'string') {
        car[key] = car[key].toUpperCase();
      }
    })
  });
}

//13
////Разработайте функцию getCarsMadeAfter, которая принимает массив объектов car 
////с тремя полями brand - марка автомобиля, model - модель, year - год выпуска
////и число -- минимальный год выпуска.
////Не используя операторов цикла функция возвращает массив, который содержит только марки 
////автомобилей, которые были выпущены позже указанного года.
////Элементы в итоговом массиве не повторяются
////Массив отсортирован по алфавиту

function uniq(array) {
  return [...new Set(array)];
}

function getCarsNewerThan(cars, year) {
  return uniq(cars
              .filter(function(car) { return car.year >= year } )
              .map(function(car) { return car.brand } )
             ).sort();
}

//14
////Разработайте функцию checkSort, которая принимает массив строк 
////и не используя циклы проверяет отсортирован массив по алфавиту или нет
////если отсортирован функция возвращает true, иначе false

function checkSort(array) {
  return array.every(function(item, i){
    return (array.length - 1 <= i) || (item <= array[i + 1]);
  });
}

//15
////Разработайте функцию contain, которая принимает массив объектов car 
////с тремя полями brand - марка автомобиля, model - модель, year - год выпуска
////и еще один объект car в качестве второго параметра
////функция возвращает true массив содержит объект с такими полями как у второго аргумента
////иначе false
////функция не использует циклы

function contain(cars, props) {
  return cars.some(function(car){
    return car.toString() === props.toString();
  });
}

//16
////Разработайте функцию getAvgAge, которая принимает массив объектов car 
////с тремя полями brand - марка автомобиля, model - модель, year - год выпуска
////и не используя циклов возвращает средний возраст автомобиля.
////для получения текущего года использовать конструкцию:
////(new Date()).getFullYear()


// 3200ms
// 2900ms
function getAvgAge(cars) {
  const year = (new Date).getFullYear();

  const sumAge = cars.reduce(function(sum, car) {
    return (year - car.year) + sum;
  }, 0);

  return Math.floor(sumAge / cars.length);
}












