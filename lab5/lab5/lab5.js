﻿'use strict'
//1
////Необходимо создать модуль, который используя функциональный стиль описывает класс CustomArray
////Конструктор может принимать неограниченное количество параметров, которые будут добавлены как элементы созданного псевдомассива
////Класс используется для создания объектов аналогичных стандартным массивам.
////Класс должен реализовывать следующие возможности массивов:
//// доступ к элементам по индексу
//// свойство length
//// методы push/pop, shift/unshift, slice, splice,  sort, indexOf/lastIndexOf, includes, join, concat
//// перебирающие методы forEach, reduce
//// Сигнатуры с действия методов аналогичны методам стандартных массивов
//// Стандартные массивы использовать ЗАПРЕЩАЕТСЯ

function CustomArray (...elements) {
  var nElements = 0;

  function isUndefined(val) {
    return val === undefined;
  }

  function defaultEquals(x, y) {
    return x === y;
  }

  this.nodeAtIndex = function(index) {
    var node, i;
    if (index < 0 || index >= nElements) {
      return undefined;
    }
    if (index === (nElements - 1)) {
      return this.lastNode;
    }
    node = this.firstNode;
    for (i = 0; i < index; i += 1) {
      node = node.next;
    }
    return node;
  };

  this.pop = function() {
    return this.removeElementAtIndex(nElements - 1);
  };

  this.shift = function() {
    return this.removeElementAtIndex(0);
  };

  this.push = function(item) {
    return this.add(item);
  };

  this.unshift = function(item) {
    return this.add(item, 0);
  };

  this.join = function(str) {
    var acc = '';
    for(var i = 0; i < (nElements - 1); i++){
      acc += this.elementAtIndex(i) + str;
    }
    acc += listElementAtIndex(nElements - 1);
    return acc;
  };

  this.concat = function(other) {
    while(!other.isEmpty()) {
      this.push(other.pop());
    }
    return this;
  };

  this.add = function (item, index) {
    var newNode, prev;

    if (isUndefined(item)) {
      return false;
    }
    if (isUndefined(index)) {
      index = nElements;
    }
    if (index < 0 || index > nElements || isUndefined(item)) {
      return false;
    }
    newNode = {
      element: item,
      next: undefined
    };
    if (nElements === 0) {
      this.firstNode = newNode;
      this.lastNode = newNode;
    } else if (index === nElements) {
      this.lastNode.next = newNode;
      this.lastNode = newNode;
    } else if (index === 0) {
      newNode.next = this.firstNode;
      this.firstNode = newNode;
    } else {
      prev = this.nodeAtIndex(index - 1);
      newNode.next = prev.next;
      prev.next = newNode;
    }
    return nElements += 1;
  };

  this.elementAtIndex = function (index) {
    var node = this.nodeAtIndex(index);
    if (node === undefined) {
      return undefined;
    }
    return node.element;
  };

  this.indexOf = function (item, equalsFunction) {
    var equalsF = equalsFunction || defaultEquals,
        currentNode = this.firstNode,
        index = 0;
    if (isUndefined(item)) {
      return -1;
    }

    while (currentNode !== undefined) {
      if (equalsF(currentNode.element, item)) {
        return index;
      }
      index += 1;
      currentNode = currentNode.next;
    }
    return -1;
  };

  this.lastIndexOf = function(item, equalsFunction) {
    return this.reverse().lastIndexOf(item, equalsFunction);
  };

  this.includes = function (item, equalsFunction) {
    return (this.indexOf(item, equalsFunction) >= 0);
  };

  this.removeElementAtIndex = function (index) {
    var element, previous;

    if (index < 0 || index >= nElements) {
      return undefined;
    }

    if (nElements === 1) {
      element = this.firstNode.element;
      this.firstNode = undefined;
      this.lastNode = undefined;
    } else {
      previous = this.nodeAtIndex(index - 1);
      if (previous === undefined) {
        element = this.firstNode.element;
        this.firstNode = this.firstNode.next;
      } else if (previous.next === this.lastNode) {
        element = this.lastNode.element;
        this.lastNode = previous;
      }
      if (previous !== undefined) {
        element = previous.next.element;
        previous.next = previous.next.next;
      }
    }
    nElements -= 1;
    return element;
  };

  this.forEach = function (callback) {
    var currentNode = this.firstNode;
    var index = 0;
    while (currentNode !== undefined) {
      if (callback(currentNode.element, index) === false) {
        break;
      }
      index += 1;
      currentNode = currentNode.next;
    }
  };

  this.reduce = function(callback, acc) {
    var currentNode = this.firstNode;
    while (currentNode !== undefined) {
      acc = callback(acc, currentNode.element);
      currentNode = currentNode.next;
    }
    return acc;
  };

  this.reverse = function () {
    var current = this.firstNode,
        previous,
        temp;
    while (current !== undefined) {
      temp = current.next;
      current.next = previous;
      previous = current;
      current = temp;
    }
    temp = this.firstNode;
    this.firstNode = this.lastNode;
    this.lastNode = temp;
  };

  this.length = function () {
    return nElements;
  };

  Object.defineProperty(this, 'length', {
    get: function() { return nElements; }
  });

  this.isEmpty = function () {
    return nElements <= 0;
  };

  this.swapElements = function(i, j) {
    if (i < j) { [i, j] = [j, i]; }
    var tmp = this.elementAtIndex(i);
    this.removeElementAtIndex(i);
    this.add(this.elementAtIndex(j), i);
    this.removeElementAtIndex(j);
    this.add(tmp, j);
    return this;
  };

  this.sort = function(compare) {
    compare = compare || function(a,b) { return a > b; };
    var done = false;
    while (! done) {
      done = true;
      for (var i = 1; i < nElements; i++) {
        if (compare(this.elementAtIndex(i - 1), this.elementAtIndex(i))) {
          done = false;
          this.swapElements(i - 1, i);
        }
      }
    }
    return this;
  };

  for(var i = 0; i < elements.length; i += 1) {
    this.push(elements[i]);
  }


  return this;
};

//разработайте пример использования

//2
////Необходимо создать модуль, который используя функциональный стиль описывает класс TypedArray
////Класс является наследником от класса CustomArray, и используется для создания псевдомассивов, клоторые содержат только элементы определенного типа.
////В качестве первого параметра конструктора. Остальные параметры конструктора -- элементы псевдомассива
////В качестве типа могут выступать примитивные типы: number, string, boolean, тип object, либо класс, например Date
////Класс должен реализовывать следующие возможности массивов:
//// доступ к элементам по индексу
//// свойство length
//// методы push/pop, shift/unshift, slice, splice,  sort, indexOf/lastIndexOf, includes, join, concat
//// перебирающие методы forEach, reduce
//// Сигнатуры с действия методов аналогичны методам стандартных массивов
//// При попытке добавления в псевдо массив элементов с другим типом должно срабатывать исключение.
//// Стандартные массивы использовать ЗАПРЕЩАЕТСЯ

function TypedArray(type, ...elements) {
  if (elements.some(function(el) { return typeof(el) !== type;})) {
    throw 'Type Error';
  }

  const that = new CustomArray(...elements);

  const superAdd = that.add.bind(that);

  that.add = function(item ,index) {
    if (type !== typeof(item)) {
      throw 'Type Error';
    }
    return superAdd(item, index);
  };

  return that;
}

//3
////Необходимо создать модуль, который используя функциональный стиль описывает класс SortedList
////Класс является наследником от класса TypedArray, и используется для создания псевдомассивов, элементы в которых отсортированы.
////Порядок сортировки задается через функцию compareFunction(a, b), которая задается через первый параметр конструктора.
////compareFunction аналогична compareFunction для метода sort класса Array. Если функция не задана, то объекты не сортируются
////второй параметр конструктора -- тип элементов псевдомассива. остальные -- элементы псевдомассива
////Класс должен реализовывать следующие возможности:
//// свойство length и методы  indexOf/lastIndexOf, includes, join -- аналогичные методам класса CustomArray
//// метод add(elem) -- добавляет элемент  в отсортированный псевдомассив. метод должен сам определять место и аставлять туда элемент.
//// (Вставка в конец и сортировка после этого -- плохой подход)
//// метод addRange() может принимать от одного и болоее аргументов. Если аргумент не массив то добавляет его в текущий псевдомассив.
//// если моссив то добавляет в псевдомассив все элементы массива
//// remove(elem) - удаляет элемент из псевдомассива, элементы сдвигаются
//// remove(pisition) - удаляет элемент из указанной позиции псевдомассива, элементы сдвигаются
//// removeRange(begin, count) -- удаляет count элементов начиная с begin позиции, элементы сдвигаются
//// elementAt(position) -- возвращает элемент с указанной позицией
//// toArray() -- возвращает массив содержащий все элементы псевдомассива
//// sort(compareFunction(a, b)) изменяет порядок элементов в псевдомассиве.
//// перебирающие методы forEach, reduce

function SortedList(compareFn, type, ...elements) {
  const that = new TypedArray(type, ...elements).sort(compareFn);

  const superAdd = that.add.bind(that);

  that.add = function(item) {
    that.forEach(function(el, index){
      if (!compareFn(item, el)){
        superAdd(item, index);
        return false;
      }
    });
    return that;
  };

  that.addRange = function(...elements) {
    elements.forEach(function(el) {
      that.add(el);
    });
  };

  that.remove = function(elemOrIndex) {
    if (typeof(elemOrIndex) !== type && typeof(elemOrIndex) === 'number') {
      return that.removeElementAtIndex(elemOrIndex);
    } else {
      return that.removeElementAtIndex(that.indexOf(elemOrIndex));
    }
  };

  that.elementAt = function(position) {
    return that.elementAtIndex(position);
  };

  that.toArray = function() {
    var result = [];
    that.forEach(function (element) {
      result.push(element);
    });
    return result;
  };
  return that;
}
//4
////Выполните задание 1 в прототипном стиле

function PCustomArray (...elements) {
  this.nElements = 0;

  for(var i = 0; i < elements.length; i += 1) {
    this.push(elements[i]);
  }
}

PCustomArray.prototype = {
  isUndefined: function(val) {
    return val === undefined;
  },
  defaultEquals: function(x, y) {
    return x === y;
  },
  nodeAtIndex: function(index) {
    var node, i;
    if (index < 0 || index >= this.nElements) {
      return undefined;
    }
    if (index === (this.nElements - 1)) {
      return this.lastNode;
    }
    node = this.firstNode;
    for (i = 0; i < index; i += 1) {
      node = node.next;
    }
    return node;
  },

  pop: function() {
    return this.removeElementAtIndex(this.nElements - 1);
  },

  shift: function() {
    return this.removeElementAtIndex(0);
  },

  push: function(item) {
    return this.add(item);
  },

  unshift: function(item) {
    return this.add(item, 0);
  },

  join: function(str) {
    var acc = '';
    for(var i = 0; i < (this.nElements - 1); i++){
      acc += this.elementAtIndex(i) + str;
    }
    acc += listElementAtIndex(this.nElements - 1);
    return acc;
  },
  concat: function(other) {
    while(!other.isEmpty()) {
      this.push(other.pop());
    }
    return this;
  },
  add: function (item, index) {
    var newNode, prev;

    if (this.isUndefined(item)) {
      return false;
    }
    if (this.isUndefined(index)) {
      index = this.nElements;
    }
    if (index < 0 || index > this.nElements || this.isUndefined(item)) {
      return false;
    }
    newNode = {
      element: item,
      next: undefined
    };
    if (this.nElements === 0) {
      this.firstNode = newNode;
      this.lastNode = newNode;
    } else if (index === this.nElements) {
      this.lastNode.next = newNode;
      this.lastNode = newNode;
    } else if (index === 0) {
      newNode.next = this.firstNode;
      this.firstNode = newNode;
    } else {
      prev = this.nodeAtIndex(index - 1);
      newNode.next = prev.next;
      prev.next = newNode;
    }
    return this.nElements += 1;
  },

  elementAtIndex: function (index) {
    var node = this.nodeAtIndex(index);
    if (node === undefined) {
      return undefined;
    }
    return node.element;
  },

  indexOf: function (item, equalsFunction) {
    var equalsF = equalsFunction || this.defaultEquals,
        currentNode = this.firstNode,
        index = 0;
    if (this.isUndefined(item)) {
      return -1;
    }

    while (currentNode !== undefined) {
      if (equalsF(currentNode.element, item)) {
        return index;
      }
      index += 1;
      currentNode = currentNode.next;
    }
    return -1;
  },

  lastIndexOf: function(item, equalsFunction) {
    return this.reverse().lastIndexOf(item, equalsFunction);
  },

  includes: function (item, equalsFunction) {
    return (this.indexOf(item, equalsFunction) >= 0);
  },

  removeElementAtIndex: function (index) {
    var element, previous;

    if (index < 0 || index >= this.nElements) {
      return undefined;
    }

    if (this.nElements === 1) {
      element = this.firstNode.element;
      this.firstNode = undefined;
      this.lastNode = undefined;
    } else {
      previous = this.nodeAtIndex(index - 1);
      if (previous === undefined) {
        element = this.firstNode.element;
        this.firstNode = this.firstNode.next;
      } else if (previous.next === this.lastNode) {
        element = this.lastNode.element;
        this.lastNode = previous;
      }
      if (previous !== undefined) {
        element = previous.next.element;
        previous.next = previous.next.next;
      }
    }
    this.nElements -= 1;
    return element;
  },

  forEach: function (callback) {
    var currentNode = this.firstNode;
    var index = 0;
    while (currentNode !== undefined) {
      if (callback(currentNode.element, index) === false) {
        break;
      }
      index += 1;
      currentNode = currentNode.next;
    }
  },

  reduce: function(callback, acc) {
    var currentNode = this.firstNode;
    while (currentNode !== undefined) {
      acc = callback(acc, currentNode.element);
      currentNode = currentNode.next;
    }
    return acc;
  },

  reverse: function () {
    var current = this.firstNode,
        previous,
        temp;
    while (current !== undefined) {
      temp = current.next;
      current.next = previous;
      previous = current;
      current = temp;
    }
    temp = this.firstNode;
    this.firstNode = this.lastNode;
    this.lastNode = temp;
  },

  isEmpty: function () {
    return this.nElements <= 0;
  },

  swapElements: function(i, j) {
    if (i < j) { [i, j] = [j, i]; }
    var tmp = this.elementAtIndex(i);
    this.removeElementAtIndex(i);
    this.add(this.elementAtIndex(j), i);
    this.removeElementAtIndex(j);
    this.add(tmp, j);
    return this;
  },

  sort: function(compare) {
    compare = compare || function(a,b) { return a > b; };
    var done = false;
    while (! done) {
      done = true;
      for (var i = 1; i < this.nElements; i++) {
        if (compare(this.elementAtIndex(i - 1), this.elementAtIndex(i))) {
          done = false;
          this.swapElements(i - 1, i);
        }
      }
    }
    return this;
  }
};


//5 
////Выполните задание 2 в прототипном стиле

function PTypedArray(type, ...elements) {
  this.type = type;

  if (elements.some(function(el) { return typeof(el) !== type;})) {
    throw 'Type Error';
  }
  PCustomArray.call(this, ...elements);
}

PTypedArray.prototype = new PCustomArray();

PTypedArray.prototype.add = function(item, index) {
  if (this.type !== typeof(item)) { throw 'Type Error'; }

  return PCustomArray.prototype.add.call(this, item, index);
};

//6
////Выполните задание 3 в прототипном стиле

function PSortedList(compareFn, type, ...elements) {
  this.compareFn = compareFn;
  PTypedArray.call(this, type, ...elements);
  PTypedArray.prototype.sort.call(this, compareFn);
}

PSortedList.prototype = new PTypedArray();

PSortedList.prototype.add = function(item) {
  var elementsBefore = this.nElements;

  this.forEach(function(el, index){
    if (!this.compareFn(item, el)){
      PTypedArray.prototype.add.call(this, item, index);
      return false;
    }
  }.bind(this));

  if (this.nElements === elementsBefore) {
    return PTypedArray.prototype.add.call(this, item);
  }
};

PSortedList.prototype.addRange = function(...elements) {
  elements.forEach(function(el) {
    this.add(el);
  });
};

PSortedList.prototype.remove = function(elemOrIndex) {
  if (typeof(elemOrIndex) !== this.type && typeof(elemOrIndex) === 'number') {
    return this.removeElementAtIndex(elemOrIndex);
  } else {
    return this.removeElementAtIndex(this.indexOf(elemOrIndex));
  }
};

PSortedList.prototype.elementAt = function(position) {
  return this.elementAtIndex(position);
};

PSortedList.prototype.toArray = function() {
  var result = [];
  this.forEach(function (element) {
    result.push(element);
  });
  return result;
};

//7
////Необходимо создать модуль, в описана библиотека JArray. Сокращенно $A.
////библиотека работает по принципу JQuery. Добавляет различные свойства и методы к массивам и псевдомассивам если такиз свойств там нет.
////библиотека добавляет к псевдомассивам методы forEach, map, filter, every, some, reduce/reduceRight
////свойство actualLength которое хранит текущее количество элементов а не номар последнего+1
////пример использования
//function(){
//	JArray.map(arguments, function(item,i, array){})
//	JArray(arguments).map(function(item,i, array){})
//	$A.map(arguments, function(item,i, array){})
//	$A(arguments).map(function(item,i, array){})
//}

function JArray(args) {
  if (args !== undefined) {
    args.forEach = Array.prototype.forEach;
    args.filter = Array.prototype.filter;
    args.map = Array.prototype.map;
    args.every = Array.prototype.some;
    args.reduce = Array.prototype.reduce;
    args.reduceRight = Array.prototype.reduceRight;
    return args;
  }
};

JArray.forEach = function(args, fn) {
  return Array.prototype.forEach.call(args, fn);
};

JArray.map = function(args, fn) {
  return Array.prototype.map.call(args, fn);
};

JArray.filter = function(args, fn) {
  return Array.prototype.filter.call(args, fn);
};
JArray.every = function(args, fn) {
  return Array.prototype.every.call(args, fn);
};
JArray.reduce = function(args, fn) {
  return Array.prototype.reduce.call(args, fn);
};
JArray.reduceRight = function(args, fn) {
  return Array.prototype.reduceRight.call(args, fn);
};


function test(){ return JArray(arguments).map(function(el){ return el * 2 }) }
//test(1,2,3)

function test2(){ return JArray.map(arguments, function(el){ return el * 2 }) }
//test2(1,2,3)

const $A = JArray;

function test3(){ return $A(arguments).map(function(el){ return el * 2 }) }
//test(1,2,3)

function test4(){ return $A.map(arguments, function(el){ return el * 2 }) }
//test2(1,2,3)
