﻿'use strict'
//1
////напишите функцию накопитель store, которая принимает параметр 
////и возвращает сумму всех параметров, переданных ей при всех вызовах.
////store(5)=5;
////store(8)=13; (в предыдущем вызове передали 5)
////изменить сумму аргументов не вызывая функции невозможно


var store = function() {
  var acc = 0;
  return function(x) {
    return acc += x;
  };
}();

//2
////Разработайте функцию createStore, которая которая позволяет создавать несколько 
////экземпляров хранишищ из первого задания

function createStore() {
  var acc = 0;
  return function (x){
    return acc += x;
  };
}


//3
////Разработайте класс Car для создания объектов, которые описывают автомобили на стоянке.
////конструктор принимает строку формата "brand-model-id-driver:Ф И О-owner:Ф И O"
////либо "brand-model-id-driver:Ф И О" если водитель и собственник это один человек
////Объект содержит следующие ствойства:
////brand -- марка (строка) (доступно для чтения и записи)
////model -- модель (строка) (доступно для чтения и записи)
////id -- гос номер (строка) (доступно для чтения и записи)
////owner -- владелец автомобиля (Объект) (доступно для чтения и записи)
////driver -- водитель (Объект) (доступно для чтения и записи)
////поля owner и driver - это объекты с полями\
////surname -- фамилия (доступно для чтения и записи)
////name -- имя (доступно для чтения и записи)
////patronymic -- отчество (доступно для чтения и записи) (может отсутствовать)
////fullName -- полное имя, например Петров Иван Иванович (доступно для чтения и записи)
////shortName -- фамилия и инициалы , например Петров И. И.

class Car {
  constructor(attrsStr) {
    var [brand, model, id, driver, owner] = attrsStr.split('-');

    this.brand = brand;
    this.model = model;
    this.id = id;

    var [_, fullName]= driver.split(':');
    var [surn, name, patr] = driver.split(' ');
    this.driver = new Person(surn, name, patr);

    if (owner) {
      [_, fullName]= owner.split(':');
      [surn, name, patr] = owner.split(' ');
      this.owner = new Person(surn, name, patr);
    } else {
      this.owner = this.driver;
    }
  }
}

class Person {
  constructor(surn,name, patr) {
    this.surname = surn;
    this.name = name;
    this.patronymic = patr;
    Object.defineProperty(this, 'fullName', {
      get: function() {
        return [this.surname, this.name, this.patronymic].join(' ');
      }
    });
    this.shortName = surn + ' ' + name[0] + '. ' + patr[0] + '.';
  }
}

// var p = Person.createFrom('James John Jameson')

// new Car('bmw-m3-222-driver:John James Johnson-owner:Patrick Pat Patterson')
// new Car('bmw-m3-222-driver:John James Johnson')

//4 Разработайте класс Parking для создания объектов описывающих автостоянки.  
////Автостояна содержит следующие поля:
////id -- уникальный номер ( только для чтения, указывается при создании стоянки)
////address --адрес ( только для чтения, указывается при создании стоянки)
////maxCount -- количество мест
////rate -- тариф за час
////carCount -- количество авто на стоянке (только для чтения)
////freePlaces -- количество свободных мест (только для чтения)
////Автостояна содержит следующие методы:
////addCar -- добавляет авто на стоянку. К автомобилю добавляется время въезда и номер местаю метод возвращает номер места
////removeCar -- убрать автомобиль со стоянки. Освобождает место, возвращает полную стоимость стоянки время в часах*тариф
////getInfo -- возвращает объект который содержит статистику по стоянке
////carCount -- количество авто на стоянке
////freePlaces -- количество свободных мест
////maxTime -- максимальное время на стоянке среди автомобилей, которые сейчас на стоянке 
////minTime -- минимальное время на стоянке среди автомобилей, которые сейчас на стоянке 
////avgTime -- среднее время на стоянке среди автомобилей, которые сейчас на стоянке 


class Parking {
  constructor(id, address, maxCount, rate) {
    this.id = function() { return id; };
    this.address = function() { return address; };
    Object.defineProperty(this, 'id', {
      writable: false,
      value: id
    });

    Object.defineProperty(this, 'address', {
      writable: false,
      value: address
    });

    this.maxCount = maxCount;
    this.rate = rate;
    this.places = Array(maxCount).fill(null);

    Object.defineProperty(this, 'cars',{
      get: function() { return this.places.filter(function(car) { return car !== null}) }
    });

    Object.defineProperty(this, 'carCount', {
      writeable: false,
      get: function() {
        return this.cars.length;
      }
    });

    Object.defineProperty(this, 'freePlaces', {
      writeable: false,
      get: function() {
        return this.maxCount - this.carCount;
      }
    });
  }
  addCar(car) {
    car['enteredAt'] = new Date();
    var freePlace = this.places.indexOf(null);
    if (freePlace !== -1){
      this.places[freePlace] = car;
    }
    return freePlace;
  }

  removeCar(carToRemove) {
    const {id} = carToRemove;
    const actualCar = this.cars.find(function(car){ return car.id === id; });
    this.places[this.places.indexOf(actualCar)] = null;
    return (new Date() - actualCar.enteredAt) / 1000 / 3600 * this.rate;
  }
  getInfo() {
    const {min, max} = this.minMaxTime();
    return 'total cars: ' + this.carCount+ ' maxTime: ' + max + ' minTime: ' + min;
  }

  minMaxTime() {
    const cars = this.cars.sort(function(carA, carB) {
      return carB.enteredAt - carA.enteredAt;
    });

    const now = new Date();

    return {
      min: (now - cars[0].enteredAt) / 1000 / 3600,
      max: (now - cars[cars.length - 1].enteredAt) / 1000 / 3600
    };
  }

  avgTime() {
    const now = new Date();

    return this.cars.reduce(function(sum, car){
      return sum + now - car.enteredAt;
    }, 0) / this.carCount();
  }
}

//5
////разработайте класс List для создания списков 
////список имеет следующие методы
////add(element) - добавляет элемент в конец списка
////addFirst(element) - добавляет элемент в начало списка
////addAt(element, position) -- добавляет элемент перед элементом на указанной позиции или в конец списка, если номер позиции больше длины списка.
////remove(element) -- удаляет элемент из списка
////removeAt(element) -- удаляет элемент из из указанной позиции. Оставшиеся элементы сдвигаются
////sort(func) -- сортирует список порядок сортировки определяется функицей(аналогично сортировке массивов)
////clear() -- чистит список
////Предусмотрите возможность вызова методов цепоской
////Например: (new List()).add().add().sort()...


const Nil = {
  isEmpty: true,
  add: function() { return this; },
  addFirst: function() { return this; },
  addAt: function() { return this; },
  remove: function() { return this; },
  removeAt: function() { return this; },
  sort: function() { return this; },
  clear: function() { return this; }
};

class List {
  constructor(head, tail = Nil) {
    this.head = head;
    this.tail = tail;
  }

  add(el) {
    if (this.tail.isEmpty) {
      return new List(this.head, new List(el));
    } else {
      return new List(this.head, this.tail.add(el));
    }
  }

  addFirst(el) {
    return new List(el, this);
  }

  addAt(el, pos) {
    if (pos == 0) {
      return new List(this.head, new List(el, this.tail));
    } else {
      return new List(this.head, this.tail.addAt(el, pos-1));
    }
  }

  remove(el) {
    if (this.head === el) {
      return this.tail;
    } else {
      return new List(this.head, this.tail.remove(el));
    }
  }

  removeAt(pos) {
    if (pos === 0) {
      return this.tail;
    } else {
      return new List(this.head, this.tail.removeAt(pos));
    }
  }

  sort(fn) {
    return this.toArray().sort(fn);
  }

  toArray() {
    var that = this;
    var result = [];

    while (!that.isEmpty) {
      result.push(that.head);
      that = that.tail;
    }

    return result;
  };

  clear() {
    return Nil;
  }
}

List.prototype.isEmpty = false;


//6
////Напишите конструктор Calculator, который создаёт расширяемые объекты-калькуляторы.
////калькулятор имеет метод calculate который принимает строку формата «ЧИСЛО операция ЧИСЛО» (2+3) и возвращает результат операции
////калькулятор имеет метод addMethod(name, func), который учит калькулятор новой операции. Он получает имя операции name и функцию от двух аргументов \
////func(a,b), которая должна её реализовывать.
////Например
//// var powerCalc = new Calculator;
////powerCalc.addMethod("*", function(a, b) {
//// return a * b;
//});
////powerCalc.addMethod("/", function(a, b) {
////  return a / b;
////});
////powerCalc.addMethod("**", function(a, b) {
////  return Math.pow(a, b);
////});
////var result = powerCalc.calculate("2 ** 3");
////alert( result ); // 8

class Calculator {
  addMethod(name, fn) {
    this[name] = fn;
    return this;
  }

  calculate(str) {
    const [x, operation, y] = str.split(' ');
    return this[operation](parseFloat(x), parseFloat(y));
  }
}

// c = new Calculator()
// c.addMethod('+', function(a,b){ return a + b } )
// c.calculate('2 + 3')






